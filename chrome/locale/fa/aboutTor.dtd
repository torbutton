<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "درباره‌ٔ Tor">

<!ENTITY aboutTor.viewChangelog.label "دیدن گزارش تغییرات">

<!ENTITY aboutTor.ready.label "کاوش کنید. بطور خصوصی.">
<!ENTITY aboutTor.ready2.label "شما برای تجربهٔ خصوصی‌ترین مرور اینترنت در جهان آماده هستید.">
<!ENTITY aboutTor.failure.label "خطایی رخ داد!">
<!ENTITY aboutTor.failure2.label "Tor با این مرورگر کار نمی‌کند.">

<!ENTITY aboutTor.search.label "جستجو با DuckDuckGo">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "سؤالات؟">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "بررسی راهنمای مرورگر Tor »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "راهنمای مرورگر Tor">

<!ENTITY aboutTor.tor_mission.label "پروژه‌ٔ Tor یک سازمان غیرانتفاعی US 501(c)(3) برای پیشبرد حقوق بشر و آزادی با ایجاد و استقرار تکنولوژی‌های ناشناس‌کننده و حریم خصوصی متن‌باز است که از در دسترس بودن و استفادهٔ نامحدود و گسترش درک علمی و عمومی آنان حمایت می‌کند.">
<!ENTITY aboutTor.getInvolved.label "مشارکت کنید »">

<!ENTITY aboutTor.newsletter.tagline "آخرین اخبار Tor را در صندوق ورودی خود دریافت کنید.">
<!ENTITY aboutTor.newsletter.link_text "ثبت‌نام برای اخبار Tor.">
<!ENTITY aboutTor.donationBanner.freeToUse "Tor برای استفاده رایگان است، این به خاطر حمایت افرادی مانند شماست.">
<!ENTITY aboutTor.donationBanner.buttonA "اکنون حمایت کنید">

<!ENTITY aboutTor.alpha.ready.label "آزمایش کنید. به طور دقیق.">
<!ENTITY aboutTor.alpha.ready2.label "شما آماده آزمایش خصوصی‌ترین تجربه مرورگر جهان هستید.">
<!ENTITY aboutTor.alpha.bannerDescription "مرورگر تور آلفا یک نسخه ناپایدار از مرورگر تور است که می‌توانید از آن برای پیش‌نمایش ویژگی‌های جدید، آزمایش عملکرد آن‌ها و ارائه بازخورد قبل از انتشار استفاده کنید.">
<!ENTITY aboutTor.alpha.bannerLink "هرگونه اشکال را در تالار گفتگوی تور گزارش کنید">

<!ENTITY aboutTor.nightly.ready.label "آزمایش کنید. به طور کامل.">
<!ENTITY aboutTor.nightly.ready2.label "شما آماده آزمایش خصوصی‌ترین تجربه مرورگر جهان هستید.">
<!ENTITY aboutTor.nightly.bannerDescription " مرورگر Tor Nightly یک نسخه ناپایدار از مرورگر Tor است که می‌توانید از آن برای پیش‌نمایش ویژگی‌های جدید، آزمایش عملکرد آن‌ها و ارائه بازخورد قبل از انتشار استفاده کنید.">
<!ENTITY aboutTor.nightly.bannerLink "هرگونه اشکال را در تالار گفتگوی تور گزارش کنید">

<!-- YEC 2022 campaign https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41303 -->
<!-- LOCALIZATION NOTE (aboutTor.yec2022.powered_by_privacy): a header for a list of things which are powered by/enabled by/possible due to privacy (each item should have positive connotations/associations in the translated languages) -->
<!ENTITY aboutTor.yec2022.powered_by_privacy "قدرت گرفته توسط PRIVACY:">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.resistance): resistance as in social/political resistance to opression, injustice, etc -->
<!ENTITY aboutTor.yec2022.resistance "مقاومت">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.change): change as in the process of social/political progress toward a better/more equitable society -->
<!ENTITY aboutTor.yec2022.change "تحول">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.freedom): freedom as in liberty, protection against exploition, imprisonment, etc -->
<!ENTITY aboutTor.yec2022.freedom "آزادی">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donate_now): Label for a button directing user to donation page-->
<!ENTITY aboutTor.yec2022.donate_now "کمک مالی در همین لحظه">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donation_matching): Please translate the 'Friends of Tor' phrase, but
also format it like the name of an organization in whichever way that is appropriate for your locale.

Please keep the currency in USD.
Thank you!
-->
<!ENTITY aboutTor.yec2022.donation_matching "کمک مالی شما توسط دوستان تور، تا مبلغ 100,000$ مورد همخوان‌سازی قرار خواهد گرفت.">
