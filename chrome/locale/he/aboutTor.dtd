<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "אודות Tor">

<!ENTITY aboutTor.viewChangelog.label "הצג יומן שינויים">

<!ENTITY aboutTor.ready.label "חקור. בפרטיות.">
<!ENTITY aboutTor.ready2.label "אתה מוכן לחוויה של הגלישה הפרטית ביותר של העולם.">
<!ENTITY aboutTor.failure.label "משהו השתבש!">
<!ENTITY aboutTor.failure2.label "Tor אינו עובד בדפדפן זה.">

<!ENTITY aboutTor.search.label "חפש עם DuckDuckGo">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "שאלות?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "בדוק את מדריך של דפדפן Tor שלנו »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "מ">
<!ENTITY aboutTor.torbrowser_user_manual.label "מדריך של דפדפן Tor">

<!ENTITY aboutTor.tor_mission.label "מיזם Tor הוא ארגון US 501(c)(3) ללא רווחים המקדם זכויות אדם וחירויות ע״י יצירה ופריסה של טכנולוגיות של אלמוניות ופרטיות בקוד פתוח וחינמי, תמיכה בזמינות ובשימוש בלתי־מוגבל שלהן, וקידום הבנה מדעית ועממית שלהן.">
<!ENTITY aboutTor.getInvolved.label "הייה מעורב »">

<!ENTITY aboutTor.newsletter.tagline "קבל את החדשות האחרונות מאת Tor ישירות לתיבה הנכנסת שלך.">
<!ENTITY aboutTor.newsletter.link_text "הירשם עבור חדשות Tor.">
<!ENTITY aboutTor.donationBanner.freeToUse "Tor חינמי לשימוש בגלל תרומות מאנשים כמוך.">
<!ENTITY aboutTor.donationBanner.buttonA "תרום עכשיו">

<!ENTITY aboutTor.alpha.ready.label "בחן. ביסודיות.">
<!ENTITY aboutTor.alpha.ready2.label "אתה מוכן לבחון את החוויה של הגלישה הפרטית ביותר של העולם.">
<!ENTITY aboutTor.alpha.bannerDescription "דפדפן Tor אלפא הוא גרסה בלתי יציבה של דפדפן Tor שאתה יכול להשתמש כדי להציג מראש מאפיינים חדשים, לבחון את הביצוע שלהם ולספק משוב לפני שחרור.">
<!ENTITY aboutTor.alpha.bannerLink "דווח על תקל בפורום Tor">

<!ENTITY aboutTor.nightly.ready.label "בחן. ביסודיות.">
<!ENTITY aboutTor.nightly.ready2.label "אתה מוכן לבחון את החוויה של הגלישה הפרטית ביותר של העולם.">
<!ENTITY aboutTor.nightly.bannerDescription "Tor Browser Nightly is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.nightly.bannerLink "דווח על תקל בפורום Tor">

<!-- YEC 2022 campaign https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41303 -->
<!-- LOCALIZATION NOTE (aboutTor.yec2022.powered_by_privacy): a header for a list of things which are powered by/enabled by/possible due to privacy (each item should have positive connotations/associations in the translated languages) -->
<!ENTITY aboutTor.yec2022.powered_by_privacy "POWERED BY PRIVACY:">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.resistance): resistance as in social/political resistance to opression, injustice, etc -->
<!ENTITY aboutTor.yec2022.resistance "RESISTANCE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.change): change as in the process of social/political progress toward a better/more equitable society -->
<!ENTITY aboutTor.yec2022.change "CHANGE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.freedom): freedom as in liberty, protection against exploition, imprisonment, etc -->
<!ENTITY aboutTor.yec2022.freedom "FREEDOM">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donate_now): Label for a button directing user to donation page-->
<!ENTITY aboutTor.yec2022.donate_now "תרמו עכשיו">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donation_matching): Please translate the 'Friends of Tor' phrase, but
also format it like the name of an organization in whichever way that is appropriate for your locale.

Please keep the currency in USD.
Thank you!
-->
<!ENTITY aboutTor.yec2022.donation_matching "התרומה שלך תושווה על ידי חברים של Tor, ‏$100,000 לכל היותר.">
