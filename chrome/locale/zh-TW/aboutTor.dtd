<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "關於 Tor">

<!ENTITY aboutTor.viewChangelog.label "檢視變更記錄">

<!ENTITY aboutTor.ready.label "探索。隱密。">
<!ENTITY aboutTor.ready2.label "您已能夠體驗全世界最私密的網路瀏覽。">
<!ENTITY aboutTor.failure.label "發生錯誤！">
<!ENTITY aboutTor.failure2.label "Tor 無法在此瀏覽器中運作。">

<!ENTITY aboutTor.search.label "使用 DuckDuckGo 搜尋">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "有什麼問題？">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "查看我們的洋蔥路由瀏覽器手冊 »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "洋蔥路由瀏覽器手冊">

<!ENTITY aboutTor.tor_mission.label "Tor Project 是一個美國 501(c)(3) 非營利組織，致力於透過開發和部署自由與開放原始碼的匿名和隱私技術，支援無限制的可用性和使用，以促進科學領域和大眾對相關技術的理解。">
<!ENTITY aboutTor.getInvolved.label "加入我們 »">

<!ENTITY aboutTor.newsletter.tagline "將 Tor 的最新消息直接傳送到您的收件匣。">
<!ENTITY aboutTor.newsletter.link_text "訂閱 Tor 的新資訊。">
<!ENTITY aboutTor.donationBanner.freeToUse "由於有像您這樣的人捐款，洋蔥路由才得以免費使用。">
<!ENTITY aboutTor.donationBanner.buttonA "立刻捐款">

<!ENTITY aboutTor.alpha.ready.label "測試。徹底地。">
<!ENTITY aboutTor.alpha.ready2.label "You’re ready to test the world’s most private browsing experience.">
<!ENTITY aboutTor.alpha.bannerDescription "Tor Browser Alpha is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.alpha.bannerLink "Report a bug on the Tor Forum">

<!ENTITY aboutTor.nightly.ready.label "測試。徹底地。">
<!ENTITY aboutTor.nightly.ready2.label "You’re ready to test the world’s most private browsing experience.">
<!ENTITY aboutTor.nightly.bannerDescription "Tor Browser Nightly is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.nightly.bannerLink "Report a bug on the Tor Forum">

<!-- YEC 2022 campaign https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41303 -->
<!-- LOCALIZATION NOTE (aboutTor.yec2022.powered_by_privacy): a header for a list of things which are powered by/enabled by/possible due to privacy (each item should have positive connotations/associations in the translated languages) -->
<!ENTITY aboutTor.yec2022.powered_by_privacy "POWERED BY PRIVACY:">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.resistance): resistance as in social/political resistance to opression, injustice, etc -->
<!ENTITY aboutTor.yec2022.resistance "RESISTANCE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.change): change as in the process of social/political progress toward a better/more equitable society -->
<!ENTITY aboutTor.yec2022.change "CHANGE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.freedom): freedom as in liberty, protection against exploition, imprisonment, etc -->
<!ENTITY aboutTor.yec2022.freedom "FREEDOM">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donate_now): Label for a button directing user to donation page-->
<!ENTITY aboutTor.yec2022.donate_now "立刻贊助">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donation_matching): Please translate the 'Friends of Tor' phrase, but
also format it like the name of an organization in whichever way that is appropriate for your locale.

Please keep the currency in USD.
Thank you!
-->
<!ENTITY aboutTor.yec2022.donation_matching "洋蔥之友組織將會捐助與您所捐助的款項相同的金額，上限為$100,000美金。">
