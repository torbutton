<!ENTITY torsettings.dialog.title "Setări Rețea Tor">
<!ENTITY torsettings.wizard.title.default "Conectare la Tor">
<!ENTITY torsettings.wizard.title.configure "Setări Rețea Tor">
<!ENTITY torsettings.wizard.title.connecting "Stabilire Conexiune">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Limba pentru Tor Browser">
<!ENTITY torlauncher.localePicker.prompt "Va rugăm sa alegeți o limbă.">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Apasă &quot;Conectare&quot; pentru a te conecta la Tor.">
<!ENTITY torSettings.configurePrompt "Apasă &quot;Configurare&quot; pentru a modifica setările de rețea dacă te afli într-o țară care blochează Tor (cum ar fi Egipt, China, Turcia) sau dacă te conectezi dintr-o rețea privată care necesită un proxy.">
<!ENTITY torSettings.configure "Configurare">
<!ENTITY torSettings.connect "Conectare">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Aștept Tor să pornească...">
<!ENTITY torsettings.restartTor "Repornește Tor">
<!ENTITY torsettings.reconfigTor "Reconfigurare">

<!ENTITY torsettings.discardSettings.prompt "Ați configurat punți Tor sau ați introdus setări proxy locale.&#160; Pentru a te conecta direct la rețeaua Tor aceste setări trebuiesc eliminate.">
<!ENTITY torsettings.discardSettings.proceed "Eliminare Setări și Conectare">

<!ENTITY torsettings.optional "Opțional">

<!ENTITY torsettings.useProxy.checkbox "Folosesc un proxy pentru a mă conecta la Internet">
<!ENTITY torsettings.useProxy.type "Tip proxy">
<!ENTITY torsettings.useProxy.type.placeholder "alege un tip de proxy">
<!ENTITY torsettings.useProxy.address "Adresa">
<!ENTITY torsettings.useProxy.address.placeholder "Adresă IP sau hostname">
<!ENTITY torsettings.useProxy.port "Port">
<!ENTITY torsettings.useProxy.username "Nume utilizator">
<!ENTITY torsettings.useProxy.password "Parola">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "Acest computer trece printr-un firewall care permite doar conexiuni către anumite porturi">
<!ENTITY torsettings.firewall.allowedPorts "Porturi permise">
<!ENTITY torsettings.useBridges.checkbox "Tor este cenzurat în țara mea">
<!ENTITY torsettings.useBridges.default "Selectează o punte integrată">
<!ENTITY torsettings.useBridges.default.placeholder "selectează o punte">
<!ENTITY torsettings.useBridges.bridgeDB "Cere o punte de la torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "Introdu caracterele din imagine">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "Obțineți o nouă provocare">
<!ENTITY torsettings.useBridges.captchaSubmit "Trimite">
<!ENTITY torsettings.useBridges.custom "Folosește o punte pe care o știu">
<!ENTITY torsettings.useBridges.label "Introdu informația despre punte dintr-o sursă de încredere">
<!ENTITY torsettings.useBridges.placeholder "scrie adresă:port (una pe linie)">

<!ENTITY torsettings.copyLog "Copiați jurnalul Tor în Clipboard">

<!ENTITY torsettings.proxyHelpTitle "Ajutor Proxy">
<!ENTITY torsettings.proxyHelp1 "Un proxy local ar putea fi necesar la conectarea prin rețeaua unei companii, școli sau universități.&#160;Dacă nu ești sigur că un proxy este necesar, vezi setările pentru Internet în alt navigator web sau verifică setările de rețea.">

<!ENTITY torsettings.bridgeHelpTitle "Ajutor Releuri de tip Punte">
<!ENTITY torsettings.bridgeHelp1 "Punțile sunt relee ne-listate care fac mai dificilă blocarea conexiunilor spre Rețeaua Tor. &#160; Fiecare tip de punte folosește o altă metodă pentru a evita cenzura. &#160; Cele de tip obfs fac traficul tău să semene cu zgomot aleatoriu, iar cele meek fac traficul să pară că se conectează la acel serviciu în loc de Tor.">
<!ENTITY torsettings.bridgeHelp2 "Datorită modului în care diverse țări încearcă să blocheze Tor, unele punți funcționează doar în anumite țări și nu în altele. &#160; Dacă nu ești sigur ce punți funcționează în țara ta, vizitează torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Te rugăm să aștepți până ce stabilim o conexiune cu rețeaua Tor. &#160; Aceasta poate dura mai multe minute.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "Conexiune">
<!ENTITY torPreferences.torSettings "Setări Tor">
<!ENTITY torPreferences.torSettingsDescription "Tor Browser vă orientează traficul prin rețeaua Tor, condusă de mii de voluntari din întreaga lume." >
<!ENTITY torPreferences.learnMore "Aflați mai multe">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Internet:">
<!ENTITY torPreferences.statusInternetTest "Test">
<!ENTITY torPreferences.statusInternetOnline "Online">
<!ENTITY torPreferences.statusInternetOffline "Offline">
<!ENTITY torPreferences.statusTorLabel "Rețeaua Tor:">
<!ENTITY torPreferences.statusTorConnected "Conectată">
<!ENTITY torPreferences.statusTorNotConnected "Neconectat">
<!ENTITY torPreferences.statusTorBlocked "Blocat potențial">
<!ENTITY torPreferences.learnMore "Aflați mai multe">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "Pornire rapidă">
<!ENTITY torPreferences.quickstartDescriptionLong "QuickStart conectează Tor Browser la rețeaua Tor automat atunci când este lansată, pe baza ultimelor setări de conexiune utilizate.">
<!ENTITY torPreferences.quickstartCheckbox "Conectați-vă automat întotdeauna">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Punți">
<!ENTITY torPreferences.bridgesDescription "Podurile vă ajută să accesați rețeaua Tor în locurile în care Tor este blocat. În funcție de locul în care vă aflați, un pod poate funcționa mai bine decât altul.">
<!ENTITY torPreferences.bridgeLocation "Locația dumneavoastră">
<!ENTITY torPreferences.bridgeLocationAutomatic "Automatic">
<!ENTITY torPreferences.bridgeLocationFrequent "Locații selectate frecvent">
<!ENTITY torPreferences.bridgeLocationOther "Alte locații">
<!ENTITY torPreferences.bridgeChooseForMe "Alege un pod pentru mine...">
<!ENTITY torPreferences.bridgeBadgeCurrent "Podurile voastre actuale">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "Puteți salva unul sau mai multe poduri, iar Tor vă alege pe care să o utilizați atunci când vă conectați. Tor va comuta automat pentru a utiliza un alt pod atunci când este necesar.">
<!ENTITY torPreferences.bridgeId "#1 pod: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "Eliminați">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Dezactivați podurile încorporate">
<!ENTITY torPreferences.bridgeShare "Partajați această punte utilizând codul QR sau copiind adresa:">
<!ENTITY torPreferences.bridgeCopy "Copiere adresă pod">
<!ENTITY torPreferences.copied "Copiat!">
<!ENTITY torPreferences.bridgeShowAll "Arată toate podurile">
<!ENTITY torPreferences.bridgeRemoveAll "Stergeți toate podurile">
<!ENTITY torPreferences.bridgeAdd "Adăugați un pod nou">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Alegeți unul dintre podurile încorporate ale Tor Browser">
<!ENTITY torPreferences.bridgeSelectBuiltin "Selectați un pod încorporat...">
<!ENTITY torPreferences.bridgeRequest "Cerere Punte...">
<!ENTITY torPreferences.bridgeEnterKnown "Introduceți o adresă de pod pe care o cunoașteți deja">
<!ENTITY torPreferences.bridgeAddManually "Adăugați manual un pod…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "Avansat">
<!ENTITY torPreferences.advancedDescription "Configurați modul în care Tor Browser se conectează la internet">
<!ENTITY torPreferences.advancedButton "Setări...">
<!ENTITY torPreferences.viewTorLogs "Vezi jurnalele Tor">
<!ENTITY torPreferences.viewLogs "Vizualizați jurnalele ...">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Stergeți toate podurile?">
<!ENTITY torPreferences.removeBridgesWarning "Această acțiune nu poate fi ireversibilă.">
<!ENTITY torPreferences.cancel "Anulați">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Scanează codul QR">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Poduri încorporate">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser include unele tipuri specifice de poduri cunoscute sub numele de &quot;transporturi conectabile&quot;.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 este un tip de pod încorporat care face ca traficul Tor să pară aleatoriu. De asemenea, este mai puțin probabil ca acestea să fie blocate decât predecesorii lor, podurile obfs3.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake este un pod încorporat care învinge cenzura prin rutarea conexiunii prin proxy-uri Snowflake, rulate de voluntari.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure este un pod încorporat care face să pară că utilizați un website Microsoft în loc să utilizați Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "Cere podul">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Contactarea BridgeDB. Te rog asteapta.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Rezolvă acest CAPTCHA pentru a cere o punte.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "Soluția nu este corectă. Încearcă din nou.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Oferă pod">
<!ENTITY torPreferences.provideBridgeHeader "Introduceți informații despre punte dintr-o sursă de încredere">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Setările de conexiune">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Configurați modul în care Tor Browser se conectează la internet">
<!ENTITY torPreferences.firewallPortsPlaceholder "Valori separate de virgulă">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Jurnalele Tor">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "Neconectat">
<!ENTITY torConnect.connectingConcise "Se conectează...">
<!ENTITY torConnect.tryingAgain "Încercăm din nou...">
<!ENTITY torConnect.noInternet "Tor Browser nu s-a putut conecta la internet">
<!ENTITY torConnect.noInternetDescription "This could be due to a connection issue rather than Tor being blocked. Check your Internet connection, proxy and firewall settings before trying again.">
<!ENTITY torConnect.couldNotConnect "Tor Browser nu s-a putut conecta la Tor">
<!ENTITY torConnect.assistDescriptionConfigure "Configurați conexiunea"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "Dacă Tor este blocat în locația dumneavoastră, încercarea unui pod poate ajuta. Connection Assist poate alege unul pentru dumneavoastră utilizând locația dumneavoastră, sau puteți #1 manual în schimb."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Încearcă un pod...">
<!ENTITY torConnect.tryingBridgeAgain "Încercăm încă o dată...">
<!ENTITY torConnect.errorLocation "Tor Browser nu v-a putut localiza">
<!ENTITY torConnect.errorLocationDescription "Tor Browser trebuie să știe locația dumneavoastră pentru a alege podul potrivit pentru dumneavoastră. Dacă preferați să nu partajați locația dumneavoastră, #1 în schimb manual."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Aceste setări de locație sunt corecte?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser încă nu s-a putut conecta la Tor. Verificați dacă setările de locație sunt corecte și încercați din nou, sau #1 în schimb."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.finalError "Tor Browser still cannot connect">
<!ENTITY torConnect.finalErrorDescription "Despite its best efforts, connection assist was not able to connect to Tor. Try troubleshooting your connection and adding a bridge manually instead.">
<!ENTITY torConnect.breadcrumbAssist "Asistență la conectare">
<!ENTITY torConnect.breadcrumbLocation "Setările de locație">
<!ENTITY torConnect.breadcrumbTryBridge "Încearcă un pod">
<!ENTITY torConnect.automatic "Automatic">
<!ENTITY torConnect.selectCountryRegion "Selectați țara sau regiunea">
<!ENTITY torConnect.frequentLocations "Locații selectate frecvent">
<!ENTITY torConnect.otherLocations "Alte locații">
<!ENTITY torConnect.restartTorBrowser "Reporniți Tor Browser">
<!ENTITY torConnect.configureConnection "Configurați conexiunea…">
<!ENTITY torConnect.viewLog "Vizualizare jurnale…">
<!ENTITY torConnect.tryAgain "Încercați din nou">
<!ENTITY torConnect.offline "Internetul nu este accesibil">
<!ENTITY torConnect.connectMessage "Schimbările la setările Tor nu vor fi aplicate până când vă conectați la Tor Network">
<!ENTITY torConnect.tryAgainMessage "Tor Browser a eșuat să stabilească o conexiune la Tor Network">
<!ENTITY torConnect.yourLocation "Locația dumneavoastră">
<!ENTITY torConnect.tryBridge "Încearcă un pod">
<!ENTITY torConnect.autoBootstrappingFailed "Configurarea automată a eșuat">
<!ENTITY torConnect.autoBootstrappingFailed "Configurarea automată a eșuat">
<!ENTITY torConnect.cannotDetermineCountry "Nu se poate determina țara utilizatorului">
<!ENTITY torConnect.noSettingsForCountry "Nu sunt disponibile setări pentru locația dumneavoastră">
