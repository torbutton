# For Tor Browser, we use a new file (different than the brand.ftl file
# that is used by Firefox) to avoid picking up the -brand-short-name values
# that Mozilla includes in the Firefox language packs.

-brand-shorter-name = Tor браузер
-brand-short-name = Tor Browser
-brand-full-name = Tor Browser
# This brand name can be used in messages where the product name needs to
# remain unchanged across different versions (Nightly, Beta, etc.).
-brand-product-name = Tor браузер
-vendor-short-name = Проект Tor
trademarkInfo = 'Tor' і 'Onion Logo' є зареєстрованими товарними знаками Tor Project, Inc.
