<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "Про Tor">

<!ENTITY aboutTor.viewChangelog.label "Переглянути список змін">

<!ENTITY aboutTor.ready.label "Шукайте. Приватно.">
<!ENTITY aboutTor.ready2.label "Ви готові до найприватнішого перегляду Інтернету в світі.">
<!ENTITY aboutTor.failure.label "Щось пішло не так!">
<!ENTITY aboutTor.failure2.label "Tor не працює у цьому браузері.">

<!ENTITY aboutTor.search.label "Пошук через DuckDuckGo">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "Питання?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "Перегляньте наш посібник для вебоглядача Tor »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "Посібник для вебоглядача Tor">

<!ENTITY aboutTor.tor_mission.label "Проєкт &quot;Tor&quot;,  згідно зі статтею 501(c)(3) Кодексу США, є неприбутковою організацією, що займається захистом прав і свобод людини, створенням і впровадженням безкоштовних технологій анонімності й приватності з відкритим кодом, підтримуючи їхні необмежені доступність і використання, а також сприяючи їх розумінню наукою і широким загалом.">
<!ENTITY aboutTor.getInvolved.label "Візьміть участь »">

<!ENTITY aboutTor.newsletter.tagline "Отримуйте останні новини Tor зразу до своєї скриньки.">
<!ENTITY aboutTor.newsletter.link_text "Підпишіться на новини Tor">
<!ENTITY aboutTor.donationBanner.freeToUse "Tor можна використовувати безкоштовно завдяки підтримці таких людей, як Ви.">
<!ENTITY aboutTor.donationBanner.buttonA "Підтримати зараз">

<!ENTITY aboutTor.alpha.ready.label "Тест. Ретельно.">
<!ENTITY aboutTor.alpha.ready2.label "Ви готові випробувати найприватніший досвід перегляду в світі.">
<!ENTITY aboutTor.alpha.bannerDescription "Браузер Tor Alpha - це нестабільна версія Браузера Tor, яку можна використовувати для попереднього перегляду нових функцій, тестування їхньої продуктивності та надання зворотного зв’язку перед випуском.">
<!ENTITY aboutTor.alpha.bannerLink "Повідомте про помилку на форумі Tor">

<!ENTITY aboutTor.nightly.ready.label "Тест. Ретельно.">
<!ENTITY aboutTor.nightly.ready2.label "Ви готові випробувати найприватніший досвід перегляду в світі.">
<!ENTITY aboutTor.nightly.bannerDescription "Tor Browser Nightly is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.nightly.bannerLink "Повідомте про помилку на форумі Tor">

<!-- YEC 2022 campaign https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41303 -->
<!-- LOCALIZATION NOTE (aboutTor.yec2022.powered_by_privacy): a header for a list of things which are powered by/enabled by/possible due to privacy (each item should have positive connotations/associations in the translated languages) -->
<!ENTITY aboutTor.yec2022.powered_by_privacy "POWERED BY PRIVACY:">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.resistance): resistance as in social/political resistance to opression, injustice, etc -->
<!ENTITY aboutTor.yec2022.resistance "RESISTANCE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.change): change as in the process of social/political progress toward a better/more equitable society -->
<!ENTITY aboutTor.yec2022.change "CHANGE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.freedom): freedom as in liberty, protection against exploition, imprisonment, etc -->
<!ENTITY aboutTor.yec2022.freedom "FREEDOM">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donate_now): Label for a button directing user to donation page-->
<!ENTITY aboutTor.yec2022.donate_now "ПІДТРИМАТИ ЗАРАЗ">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donation_matching): Please translate the 'Friends of Tor' phrase, but
also format it like the name of an organization in whichever way that is appropriate for your locale.

Please keep the currency in USD.
Thank you!
-->
<!ENTITY aboutTor.yec2022.donation_matching "Ваша пожертва надійде Друзям Tor (до 100 000 доларів США).">
