<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "عن تور">

<!ENTITY aboutTor.viewChangelog.label "الإطلاع على سجل التغيرات">

<!ENTITY aboutTor.ready.label "تصفح بكل خصوصية.">
<!ENTITY aboutTor.ready2.label "أنت جاهز الآن لتجربة التصفح الأكثر خصوصية في العالم.">
<!ENTITY aboutTor.failure.label "حدث خطأ ما!">
<!ENTITY aboutTor.failure2.label "تور لا يعمل في هذا المتصفح.">

<!ENTITY aboutTor.search.label "ابحث بواسطة داك داك غو">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "أية أسئلة؟">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "اطّلع على دليل استخدام متصفح تور ">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "س">
<!ENTITY aboutTor.torbrowser_user_manual.label "دليل استخدام متصفّح تور">

<!ENTITY aboutTor.tor_mission.label "مشرع تور منظمة غير ربحية أميريكية خاضعة لقانون 501(c)(3)، تعمل على تقدم حقوق الإنسان وحرياته، عبر إنشاء واستخدام تقنيات حرة ومفتوحة المصدر للمجهولية والخصوصية، وتدعم توفرها واستخدامها دون قيود، وتدعم تقدم الفهم العلمي والشعبي لهذه التقنيات.">
<!ENTITY aboutTor.getInvolved.label "شارك">

<!ENTITY aboutTor.newsletter.tagline "احصل على آخر أخبار تور مباشرة على بريدك">
<!ENTITY aboutTor.newsletter.link_text "اشترك للحصول على أخبار تور.">
<!ENTITY aboutTor.donationBanner.freeToUse "Tor متاح مجاناً بفضل تبرعات من أشخاص مثلك">
<!ENTITY aboutTor.donationBanner.buttonA "تبرع الآن">

<!ENTITY aboutTor.alpha.ready.label "اختبار. بدقة.">
<!ENTITY aboutTor.alpha.ready2.label "أنت على استعداد لاختبار تجربة التصفح الأكثر خصوصية في العالم.">
<!ENTITY aboutTor.alpha.bannerDescription "Tor Browser Alpha هو إصدار غير مستقر من متصفح Tor يمكنك استخدامه لمعاينة الميزات الجديدة واختبار أدائها وتقديم الملاحظات قبل الإصدار.">
<!ENTITY aboutTor.alpha.bannerLink "أبلغ عن خطأ في منتدى Tor">

<!ENTITY aboutTor.nightly.ready.label "اختبار. بدقة.">
<!ENTITY aboutTor.nightly.ready2.label "أنت على استعداد لاختبار تجربة التصفح الأكثر خصوصية في العالم.">
<!ENTITY aboutTor.nightly.bannerDescription "Tor Browser Nightly هو إصدار غير مستقر من متصفح Tor يمكنك استخدامه لمعاينة الميزات الجديدة واختبار أدائها وتقديم الملاحظات قبل الإصدار.">
<!ENTITY aboutTor.nightly.bannerLink "أبلغ عن خطأ في منتدى Tor">

<!-- YEC 2022 campaign https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41303 -->
<!-- LOCALIZATION NOTE (aboutTor.yec2022.powered_by_privacy): a header for a list of things which are powered by/enabled by/possible due to privacy (each item should have positive connotations/associations in the translated languages) -->
<!ENTITY aboutTor.yec2022.powered_by_privacy "POWERED BY PRIVACY:">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.resistance): resistance as in social/political resistance to opression, injustice, etc -->
<!ENTITY aboutTor.yec2022.resistance "RESISTANCE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.change): change as in the process of social/political progress toward a better/more equitable society -->
<!ENTITY aboutTor.yec2022.change "CHANGE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.freedom): freedom as in liberty, protection against exploition, imprisonment, etc -->
<!ENTITY aboutTor.yec2022.freedom "FREEDOM">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donate_now): Label for a button directing user to donation page-->
<!ENTITY aboutTor.yec2022.donate_now "تبرع الآن">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donation_matching): Please translate the 'Friends of Tor' phrase, but
also format it like the name of an organization in whichever way that is appropriate for your locale.

Please keep the currency in USD.
Thank you!
-->
<!ENTITY aboutTor.yec2022.donation_matching "سيتم مطابقة تبرعك بواسطة Friends of Tor ، حتى 100000 دولار.">
