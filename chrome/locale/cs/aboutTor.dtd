<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "O Toru">

<!ENTITY aboutTor.viewChangelog.label "Historie změn">

<!ENTITY aboutTor.ready.label "Prohlížejte v soukromí.">
<!ENTITY aboutTor.ready2.label "Vše je připraveno pro maximální soukromí Vašeho prohlížení.">
<!ENTITY aboutTor.failure.label "Něco se pokazilo!">
<!ENTITY aboutTor.failure2.label "Tor v tomto prohlížeči nefunguje.">

<!ENTITY aboutTor.search.label "Vyhledejte s DuckDuckGo">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "Otázky?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "Podívejte se do naší příručky k prohlížeči Tor »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "Příručka prohlížeče Tor">

<!ENTITY aboutTor.tor_mission.label "Tor Project je nezisková organizace podle US 501(c)(3), která prosazuje lidská práva a svobodu vytvářením svobodných a otevřených technologií podporující anonymitu a soukromí, bez omezení jejich dostupnosti a používání, včetně podpory jejich vědeckého a všeobecného rozvoje.">
<!ENTITY aboutTor.getInvolved.label "Zapojte se »">

<!ENTITY aboutTor.newsletter.tagline "Nechte si posílat nejnovější informace o Toru.">
<!ENTITY aboutTor.newsletter.link_text "Přihlaste se k odběru zpravodaje Toru.">
<!ENTITY aboutTor.donationBanner.freeToUse "Tor je zdarma k použití díky darům od lidí jako jste vy.">
<!ENTITY aboutTor.donationBanner.buttonA "Přispějte">

<!ENTITY aboutTor.alpha.ready.label "Otestovat. Důkladně.">
<!ENTITY aboutTor.alpha.ready2.label "Jste připraveni otestovat nejsoukromější prohlížení internetu.">
<!ENTITY aboutTor.alpha.bannerDescription "Prohlížeč Tor Alpha je nestabilní verze Prohlížeč Tor, kterou můžete použít pro poznání a otestování výkonu nových funkcí a poskytnutí zpětné vazby vývojářům ještě než funkce vydají pro širokou veřejnost.">
<!ENTITY aboutTor.alpha.bannerLink "Nahlásit chybu na Fóru Tor">

<!ENTITY aboutTor.nightly.ready.label "Otestovat. Důkladně.">
<!ENTITY aboutTor.nightly.ready2.label "Jste připraveni otestovat nejsoukromější prohlížení internetu.">
<!ENTITY aboutTor.nightly.bannerDescription "Prohlížeč Tor Nightly je nestabilní verze Prohlížeč Tor, kterou můžete využít k vyzkoušení nových funkcí, otestování jejich výkonu a následně se podělte o své zkušenosti před vydáním stabilní verze.">
<!ENTITY aboutTor.nightly.bannerLink "Nahlásit chybu na Fóru Tor">