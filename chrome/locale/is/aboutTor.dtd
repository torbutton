<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "Um Tor">

<!ENTITY aboutTor.viewChangelog.label "Skoða breytingaannál">

<!ENTITY aboutTor.ready.label "Vafraðu í friði fyrir hnýsni.">
<!ENTITY aboutTor.ready2.label "Nú er allt klárt fyrir mestu fáanlegu vernd við vafur á netinu.">
<!ENTITY aboutTor.failure.label "Eitthvað fór úrskeiðis!">
<!ENTITY aboutTor.failure2.label "Tor virkar ekki í þessum vafra.">

<!ENTITY aboutTor.search.label "Leita með DuckDuckGo">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "Spurningar?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "Skoðaðu notendahandbók Tor-vafrans »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "Notendahandbók Tor-vafrans">

<!ENTITY aboutTor.tor_mission.label "Tor-verkefnið er skráð sem bandarísk US 501(c)(3) sjálfboðaliðasamtök án gróðamarkmiða, samtök sem hafa mannréttindi og frelsi að leiðarljósi og stefna að þeim markmiðum með gerð og útbreiðslu á tækni til verndar persónuupplýsinga. Sú tækni eigi að vera öllum frjáls og heimil til notkunar, vera opin til skoðunar, enda sé eitt markmiðanna að stuðla að bættum almennum og vísindalegum skilningi á þessum málum.">
<!ENTITY aboutTor.getInvolved.label "Taktu þátt »">

<!ENTITY aboutTor.newsletter.tagline "Fáðu nýjustu fréttir af Tor beint í pósthólfið þitt.">
<!ENTITY aboutTor.newsletter.link_text "Skráðu þig til að fá Tor-fréttir.">
<!ENTITY aboutTor.donationBanner.freeToUse "Öllum er frjálst að nota Tor vegna styrkja frá fólki eins og þér.">
<!ENTITY aboutTor.donationBanner.buttonA "Styrkja núna">

<!ENTITY aboutTor.alpha.ready.label "Prófa. Vandlega.">
<!ENTITY aboutTor.alpha.ready2.label "Þú ert í standi til að prófa heimsins öruggasta netvafur.">
<!ENTITY aboutTor.alpha.bannerDescription "Alfa-útgáfan Tor Browser Alpha er óstöðug útgáfa Tor-vafrans sem þú getur notað til að fá smjörþefinn af nýjum eiginleikum, prófa virkni þeirra og gefa umsögn um þá áður en að útgáfu þeirra kemur.">
<!ENTITY aboutTor.alpha.bannerLink "Tilkynna um villu á spjallsvæði Tor">

<!ENTITY aboutTor.nightly.ready.label "Prófa. Vandlega.">
<!ENTITY aboutTor.nightly.ready2.label "Þú ert í standi til að prófa heimsins öruggasta netvafur.">
<!ENTITY aboutTor.nightly.bannerDescription "Sólarhrings-útgáfan Tor Browser Nightly er óstöðug útgáfa Tor-vafrans sem þú getur notað til að fá smjörþefinn af nýjum eiginleikum, prófa virkni þeirra og gefa umsögn um þá áður en að útgáfu þeirra kemur.">
<!ENTITY aboutTor.nightly.bannerLink "Tilkynna um villu á spjallsvæði Tor">

<!-- YEC 2022 campaign https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41303 -->
<!-- LOCALIZATION NOTE (aboutTor.yec2022.powered_by_privacy): a header for a list of things which are powered by/enabled by/possible due to privacy (each item should have positive connotations/associations in the translated languages) -->
<!ENTITY aboutTor.yec2022.powered_by_privacy "PERSÓNUVERND Í FYRSTA SÆTI:">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.resistance): resistance as in social/political resistance to opression, injustice, etc -->
<!ENTITY aboutTor.yec2022.resistance "MÓTSPYRNA">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.change): change as in the process of social/political progress toward a better/more equitable society -->
<!ENTITY aboutTor.yec2022.change "FRAMÞRÓUN">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.freedom): freedom as in liberty, protection against exploition, imprisonment, etc -->
<!ENTITY aboutTor.yec2022.freedom "FRELSI">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donate_now): Label for a button directing user to donation page-->
<!ENTITY aboutTor.yec2022.donate_now "STYRKJA NÚNA">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donation_matching): Please translate the 'Friends of Tor' phrase, but
also format it like the name of an organization in whichever way that is appropriate for your locale.

Please keep the currency in USD.
Thank you!
-->
<!ENTITY aboutTor.yec2022.donation_matching "Styrkurinn þinn verður jafnaður upp af Friends of Tor, allt að 100.000 USD.">
