<!ENTITY torsettings.dialog.title "Tor ကွန်ယက် အပြင်အဆင်များ">
<!ENTITY torsettings.wizard.title.default "Tor နှင့် ချိတ်ဆက်မယ်">
<!ENTITY torsettings.wizard.title.configure "Tor ကွန်ယက် အပြင်အဆင်များ">
<!ENTITY torsettings.wizard.title.connecting "ချိတ်ဆက်မှုလိုင်း တည်ထောင်နေသည်">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Tor ဘရောင်ဇာ ဘာသာစကား">
<!ENTITY torlauncher.localePicker.prompt "ကျေးဇူးပြု၍ ဘာသာစကားရွေးချယ်ပါ">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Tor နှင့် ချိတ်ဆက်ရန် &quot;ချိတ်ဆက်မယ်&quot; ဆိုသည့် ခလုတ်အား နှိပ်ပါ">
<!ENTITY torSettings.configurePrompt "ကွန်ယက် အပြင်အဆင်များ ချိန်ညှိရန် &quot;စီစဥ်မယ်&quot; ဆိုသည့်ခလုတ်အား နှိပ်ပါ">
<!ENTITY torSettings.configure "စီစဥ်မယ်">
<!ENTITY torSettings.connect "ချိတ်ဆက်မယ်">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Tor စဖို့ စောင့်နေပါသည်...">
<!ENTITY torsettings.restartTor "Tor ကို ပြန်လည်စတင်မယ်">
<!ENTITY torsettings.reconfigTor "ပြန်ချိန်ညှိမယ်">

<!ENTITY torsettings.discardSettings.prompt "Tor ချိတ်ဆက်တံတားများအား သင်စီမံထားရှိပြီးပါပြီ သို့မဟုတ် လိုကယ်ကြားခံ proxy များ၏ အပြင်အဆင်များကို ရိုက်ထည့်ပြီးပါပြီ။ ​&#160; Tor ကွန်ယက်နှင့် တိုက်ရိုက်ချိတ်ဆက်ရန် ၎င်းအပြင်အဆင်များကို ဖယ်ရှားရပါမည်။">
<!ENTITY torsettings.discardSettings.proceed "အပြင်အဆင်များ ဖယ်ရှားပြီး ချိတ်ဆက်မယ်">

<!ENTITY torsettings.optional "မဖြစ်မနေမဟုတ်">

<!ENTITY torsettings.useProxy.checkbox "ကျွန်ုပ်သည် ကြားခံ proxy သုံး၍ အင်တာနက်နှင့် ချိတ်ဆက်ပါသည်">
<!ENTITY torsettings.useProxy.type "ကြားခံ proxy အမျိုးအစား">
<!ENTITY torsettings.useProxy.type.placeholder "ကြားခံ proxy အမျိုးအစားတစ်ခု ရွေးပါ">
<!ENTITY torsettings.useProxy.address "လိပ်စာ">
<!ENTITY torsettings.useProxy.address.placeholder "IP လိပ်စာ သို့မဟုတ် ဧည့်ခံသူအမည် (hostname)">
<!ENTITY torsettings.useProxy.port "Port">
<!ENTITY torsettings.useProxy.username "အသုံးပြုသူအမည်">
<!ENTITY torsettings.useProxy.password "စကားဝှက်">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "အချို့သော port များနှင့်သာ ချိတ်ဆက်မှုများခွင့်ပြုသည့် အသွားအလာထိန်းချုပ်သည့်အလွှာ (firewall) အား ဤကွန်ပျူတာသည် ဖြတ်သန်းပါသည်။">
<!ENTITY torsettings.firewall.allowedPorts "ခွင့်ပြုထားသော ports">
<!ENTITY torsettings.useBridges.checkbox "ကျွန်ုပ်၏ နိုင်ငံတွင် Tor ကို ဆင်ဆာခံထားပါသည်">
<!ENTITY torsettings.useBridges.default "ပင်ကိုသွင်းထားသော ချိတ်ဆက်တံတား ရွေးပါ">
<!ENTITY torsettings.useBridges.default.placeholder "ချိတ်ဆက်တံတား ရွေးပါ">
<!ENTITY torsettings.useBridges.bridgeDB "torproject.org မှ ချိတ်ဆက်တံတား တောင်းဆိုပါ">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "ရုပ်ပုံထဲမှ စာလုံးများကို ရိုက်ထည့်ပါ">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "စိန်ခေါ်မှုအသစ် ယူမယ်">
<!ENTITY torsettings.useBridges.captchaSubmit "တင်သွင်းပါ။">
<!ENTITY torsettings.useBridges.custom "ကျွန်ုပ်သိသော ချိတ်ဆက်တံတား ပံ့ပိုးပါ">
<!ENTITY torsettings.useBridges.label "ယုံကြည်ရသော အရင်းအမြစ်မှ ချိတ်ဆက်တံတား အချက်အလက် ရိုက်ထည့်ပါ">
<!ENTITY torsettings.useBridges.placeholder "လိပ်စာအမျိုးအစား : port (တစ်ကြောင်းတစ်ခုစီ)">

<!ENTITY torsettings.copyLog "Tor မှတ်တမ်းကို စာဘုတ်ပေါ်သို့ ကော်ပီကူးယူမယ်">

<!ENTITY torsettings.proxyHelpTitle "Proxy ကြားခံ အကူအညီ">
<!ENTITY torsettings.proxyHelp1 "ကုမ္ပဏီ၊ ကျောင်း၊ သို့မဟုတ် တက္ကသိုလ် ကွန်ယက်များနှင့် ချိတ်ဆက်လျှင် လိုကယ် ကြားခံ proxy လိုအပ်နိုင်ပါသည်။ &#160; သင်မှ ကြားခံ proxy လိုမလိုခြင်း မသေချာပါက အခြား ဘရောင်ဇာမှ အင်တာနက် အပြင်အဆင်များ သို့မဟုတ် သင့်စနစ်၏ ကွန်ယက်ချိတ်ဆက်မှု အပြင်အဆင်များကို လေ့လာကြည့်ရှုနိုင်ပါသည်။">

<!ENTITY torsettings.bridgeHelpTitle "ချိတ်ဆက်တံတား လက်ဆင့်ကမ်းခြင်း အကူအညီ">
<!ENTITY torsettings.bridgeHelp1 "ချိန်ဆက်တံတားများ သည် Tor ကွန်ယက်ချိတ်ဆက်မှုများ ကို ပိတ်ပယ်ရန် ခက်ခဲအောင်လုပ်သည့် စာရင်းမသွင်းထားသော လက်ဆင့်ကမ်းမှုများ ဖြစ်ပါသည်။ .&#160; ဆင်ဆာခံရခြင်းကို ရှောင်ကျင်ရန် ချိတ်ဆက်တံတား အမျိုးအစားများတစ်ခုစီသည် ကွဲပြားသော နည်းလမ်းများ အသုံးပြုပါသည်။ .&#160; Obfs အမျိုးအစားသည် သင့် အင်တာနက် အသွားအလာများကို ကျပန်းဆူညံသံကဲ့သို့ အယောင်ဆောင်ပြုပါသည်၊ Meeks အမျိုးအစားသည် သင့် အင်တာနက် အသွားအလာများ သို့မဟုတ် Tor နှင့် ချိတ်ဆက်ထားခြင်းကို အခြားဝန်ဆောင်မှုနှင့် ချိတ်ဆက်ထားခြင်းကဲ့သို့ အယောင်ဆောင်ပြုပါသည်။">
<!ENTITY torsettings.bridgeHelp2 "အချို့သောနိုင်ငံများတွင် Tor ကို ပိတ်ပယ်ရန် ကြိုးစားခြင်းကြောင့် အချို့သော ချိတ်ဆက်တံတားများသည် အချို့နိုင်ငံတွင်သာ အလုပ်လုပ်ပြီး အခြားနိုင်ငံများတွင် အလုပ်မလုပ်ပါ။ ​&#160; သင့်နိုင်ငံတွင် မည်သည့် ချိတ်ဆက်တံတားများက အလုပ်လုပ်သည်ကို မသေချာလျှင် torproject.org/about/contact.html သို့ ဝင်ရောက်ကြည့်ရှုပေးပါ။ #support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "ကျွန်ုပ်တို့မှ​ Tor ကွန်ယက်နှင့် ချိတ်ဆက်မှု ထူထောင်နေတုန်း ခေတ္တစောင့်ပေးပါ။ &#160; ဤလုပ်ဆောင်မှုသည် မိနစ်အနည်းအငယ်ကြာနိုင်လိမ့်မည်။">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "ချိတ်ဆက်ခြင်း">
<!ENTITY torPreferences.torSettings "Tor အပြင်အဆင်များ">
<!ENTITY torPreferences.torSettingsDescription "ကမ္ဘာအနှံ့ရှိ လုပ်အားပေးများ လုပ်ဆောင်သော Tor ကွန်ယက်ပေါ်တွင် သင့်အသွားအလာများကို Tor ဘရောင်ဇာမှ လမ်းကြောင်းပေးပါသည်။" >
<!ENTITY torPreferences.learnMore "ထပ်မံလေ့လာရန်">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "အင်တာနက်">
<!ENTITY torPreferences.statusInternetTest "စမ်းသပ်ရန်">
<!ENTITY torPreferences.statusInternetOnline "လိုင်းတက်နေသည်">
<!ENTITY torPreferences.statusInternetOffline "အော့ဖ်လိုင်း">
<!ENTITY torPreferences.statusTorLabel "Tor ကွန်ယက်">
<!ENTITY torPreferences.statusTorConnected "ချိတ်ဆက်ပြီးပါပြီ">
<!ENTITY torPreferences.statusTorNotConnected "ချိတ်ဆက်ထားခြင်း မရှိပါ။">
<!ENTITY torPreferences.statusTorBlocked "ပိတ်ဆို့ခံရနိုင်ချေရှိသည်။">
<!ENTITY torPreferences.learnMore "ထပ်မံလေ့လာရန်">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "လျင်မြန်စွာစတင်ခြင်း ">
<!ENTITY torPreferences.quickstartDescriptionLong "လျင်မြန်စွာစတင်ခြင်းကို စတင်လိုက်သည်နှင့် သင်နောက်ဆုံးအသုံးပြုခဲ့သော ချိတ်ဆက်မှု အပြင်အဆင်မှုပေါ်မူတည်၍  ၎င်းမှ Tor ဘရောင်ဇာနှင့် Tor ကွန်ရက်သို့ အလိုအလျောက်ချိတ်ဆက်ပေးသည် ။  ">
<!ENTITY torPreferences.quickstartCheckbox "အမြဲတမ်း အလိုအလျောက် ချိတ်ဆက်မည် ။ ">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Bridges">
<!ENTITY torPreferences.bridgesDescription "ချိတ်ဆက်တံတားများသည် Tor ကို ပိတ်ပယ်ထားသော နေရာများတွင် Tor ကွန်ယက်နှင့် ချိတ်ဆက်ရန် ကူညီပေးပါသည်။ သင့် တည်နေရာပေါ်မူတည်၍ ချိတ်ဆက်တံတားတစ်ခုသည် နောက်တစ်ခုထက် ပိုကောင်းနိုင်ပါလိမ့်မည်။">
<!ENTITY torPreferences.bridgeLocation "သင်၏ တည်နေရာ">
<!ENTITY torPreferences.bridgeLocationAutomatic "အလိုအလျောက်">
<!ENTITY torPreferences.bridgeLocationFrequent "ရွေးချယ်လေ့ရှိသော တည်နေရာများ">
<!ENTITY torPreferences.bridgeLocationOther "အခြား တည်နေရာများ">
<!ENTITY torPreferences.bridgeChooseForMe "ကျွနု်ပ်အတွက် တံတား ရွေးချယ်ပါ ။ ">
<!ENTITY torPreferences.bridgeBadgeCurrent "သင်၏ လက်ရှိ တံတားများ ">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "သင် တံတား တစ်ခုထက်ပို၍ သိမ်းထားနိုင်သည် ။ ဒါမှမဟုတ် Tor မှ သင်ချိတ်ဆက်သည့်အခါ မည်သည့်တံတားကို  အသုံးပြုရမည်ဆိုသည်ကို ရွေးချယ်ပေးလိမ့်မည် ။ Tor မှ လိုအပ်ပါက အခြား တံတားကို အသုံးပြုရန် အလိုအလျောက် ပြောင်းပေးမည် ဖြစ်သည် ။ ">
<!ENTITY torPreferences.bridgeId "#1 တံတား #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "ဖယ်ရှားရန်">
<!ENTITY torPreferences.bridgeDisableBuiltIn "နဂိုရှိ တံတားများကို ပိတ်ထားရန် ">
<!ENTITY torPreferences.bridgeShare "QR ကုဒ် ကို အသုံးပြု၍  သို့မဟုတ် လိပ်စာကို ကူးထည့်ခြင်းဖြင့် ဤ တံတား ကို ဝေမျှပါ ။">
<!ENTITY torPreferences.bridgeCopy "တံတား လိပ်စာ ကို ကူးပါ ။ ">
<!ENTITY torPreferences.copied "ကော်ပီကူးပြီး!">
<!ENTITY torPreferences.bridgeShowAll "တံတား အားလုံးကို ပြပါ ။">
<!ENTITY torPreferences.bridgeRemoveAll "တံတားအားလုံးကို ဖယ်ရှားပါ။">
<!ENTITY torPreferences.bridgeAdd "တံတား အသစ် ထည့်ပါ ။ ">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Tor ဘရောင်ဇာ ၏ နဂိုပါ တံတားများမှ ရွေးချယ်ပါ ။ ">
<!ENTITY torPreferences.bridgeSelectBuiltin "နဂိုပါ တံတား တစ်ခုကို ရွေးချယ်ပါ။ ">
<!ENTITY torPreferences.bridgeRequest "ချိတ်ဆက်တံတား တောင်းဆိုပါ...">
<!ENTITY torPreferences.bridgeEnterKnown "သင် သိရှိထားပြီးသော တံတားလိပ်စာကို ဖြည့်သွင်းပါ">
<!ENTITY torPreferences.bridgeAddManually "တံတားအား ကိုယ်တိုင် ထည့်ပါ။">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "အဆင့်မြင့်">
<!ENTITY torPreferences.advancedDescription "Tor Browser သည် အင်တာနက်နှင့် မည်သို့ ချိတ်ဆက်မည်ကို ချိန်ညှိပါ။">
<!ENTITY torPreferences.advancedButton "ဆက်တင်များ">
<!ENTITY torPreferences.viewTorLogs "Tor မှတ်တမ်းများ ကိုကြည့်ရှုပါ">
<!ENTITY torPreferences.viewLogs "မှတ်တမ်းများ ကြည့်ရှုမယ်...">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "တံတားအားလုံးကို ဖယ်ရှားမည်လား။">
<!ENTITY torPreferences.removeBridgesWarning "ထိုလုပ်ဆောင်ချက်အား ပြန်ပြင်၍ မရနိုင်ပါ။">
<!ENTITY torPreferences.cancel "ပယ်ဖျက်မည်">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "QR ကုဒ်ကို စကင်န်ဖတ်ပါ။">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "နဂိုရှိ တံတားများ">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser တွင် &quot;pluggable transports&quot; ဟုလူသိများသော သီးခြားတံတားအမျိုးအစားအချို့ ပါဝင်သည်။">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 သည် သင့် Tor ၏ အသွားအလာကို ကျပန်း ဖြစ်နိုင်စေသည့် နဂိုပါ တံတားအမျိုးအစားတစ်ခုဖြစ်သည်။ ၎င်းတို့သည် ၎င်းတို့၏ ယခင်နှစ်များဖြစ်သော obfs3 တံတားများထက် ပိတ်ဆို့ခံရနိုင်ခြေ နည်းပါသည်။">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake သည် စေတနာ့ဝန်ထမ်းများ လုပ်ဆောင်ပေးနေသော Snowflake proxies များမှတစ်ဆင့် သင်၏ ချိတ်ဆက်မှုကို လမ်းကြောင်းပြခြင်းဖြင့် စီစစ်ဖြတ်တောက်မှုကို ချေဖျက်နိုင်သည့် နဂိုပါသည့် တံတားတစ်ခု ဖြစ်သည်။">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure ">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure သည် သင် Tor ကို အသုံးပြုသည့်အစား Microsoft ဝက်ဘ်ဆိုဒ်ကို အသုံးပြုနေသယောင် ဖြစ်သည့် နဂိုပါ တံတား ဖြစ်သည် ။ ">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "ချိတ်ဆက်တံတား တောင်းဆိုမယ်">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "BridgeDB နှင့် ဆက်သွယ်နေသည်။ ခေတ္တစောင့်ပေးပါ။">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "လူဟုတ်မဟုတ်စစ်ဆေးမှုကို ဖြေ၍ bridge အား တောင်းဆိုပါ">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "ဖြေရှင်းမှုသည် မမှန်ကန်ပါ။ ပြန်စမ်းကြည့်ပေးပါ။">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "တံတား ကို ထောက်ပံ့ပေးပါ ">
<!ENTITY torPreferences.provideBridgeHeader "ယုံကြည်စိတ်ချရသည့် အရင်းအမြစ်မှ တံတား နှင့် ပတ်သက်သည့် အချက်အလက်များကို ဝင်ရောက်သည်">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "ချိတ်ဆက်မှု ဆက်တင်များ">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Tor Browser သည် အင်တာနက်နှင့် မည်သို့ ချိတ်ဆက်မည်ကို ချိန်ညှိပါ။">
<!ENTITY torPreferences.firewallPortsPlaceholder "ကော်မာခြားထားသော တန်ဖိုးများ">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Tor မှတ်တမ်းများ">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "ချိတ်ဆက်ထားခြင်း မရှိပါ။">
<!ENTITY torConnect.connectingConcise "ချိတ်ဆက်နေသည်…">
<!ENTITY torConnect.tryingAgain "ထပ်မံ လုပ်ဆောင်နေသည်">
<!ENTITY torConnect.noInternet "Tor ဘရောဇာ သည် အင်တာနက်ချိတ်ဆက်၍ မရပါ။">
<!ENTITY torConnect.noInternetDescription "This could be due to a connection issue rather than Tor being blocked. Check your Internet connection, proxy and firewall settings before trying again.">
<!ENTITY torConnect.couldNotConnect "Tor ဘရောင်ဇာ မှ Tor သို့ ချိတ်ဆက်မှု မပြုလုပ်နိုင်ပါ ။ ">
<!ENTITY torConnect.assistDescriptionConfigure "သင်၏ ချိတ်ဆက်မှု ကို ချိန်ညှိပါ ။ "> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "သင်၏ တည်နေရာတွင် Tor ကို ပိတ်ဆို့ထားပါက တံတားကို ကြိုးစား အသုံးပြုခြင်းက ကူညီနိုင်လိမ့်မည် ။ ချိတ်ဆက်မှုအကူအညီမှ  သင်၏ တည်နေရာကို အသုံးပြုကာ သင်အတွက် ရွေးချယ်ပေးနိုင်သည် သို့မဟုတ် သင်  #1 ကို ကိုယ်တိုင် လုပ်ဆောင်နိုင်သည် ။ "> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "တံတားကို ကြိုးစားချိတ်ဆက်နေခြင်း ">
<!ENTITY torConnect.tryingBridgeAgain "နောက်တစ်ကြိမ် ထပ်၍ ကြိုးစားချိတ်ဆက်ကြည့်ခြင်း ">
<!ENTITY torConnect.errorLocation "Tor ဘရောင်ဇာမှ သင်အား မရှာနိုင်ပါ ။ ">
<!ENTITY torConnect.errorLocationDescription "Tor ဘရောင်ဇာမှ သင့်အတွက် မှန်ကန်သော တံတား ရွေးချယ်ပေးနိုင်ရန် အလို့ငှာ သင်၏ တည်နေရာကို သိရှိရန်လိုအပ်သည် ။ သင်၏ တည်နေရာ ကို မဝေမျှလိုပါက သင်ကိုယ်တိုင် #1 ကို လုပ်ဆောင်ပါ ။ "> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "ဤ တည်နေရာ အပြင်အဆင်များမှာ မှန်ကန်ပါသလား ? ">
<!ENTITY torConnect.isLocationCorrectDescription "Tor ဘရောင်ဇာသည် Tor ကို ချိတ်ဆက်မှု မပြုလုပ်နိုင်ပါ ။ ကျေးဇူးပြု၍ သင် ၏ တည်နေရာ အပြင်အဆင် မှန်မမှန်ကို စစ်ဆေးပြီး ပြန်ကြိုးစား ချိတ်ဆက်ပါ ။ သို့မဟုတ် #1 ကို လုပ်ဆောင်ပါ ။ "> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.finalError "Tor Browser still cannot connect">
<!ENTITY torConnect.finalErrorDescription "Despite its best efforts, connection assist was not able to connect to Tor. Try troubleshooting your connection and adding a bridge manually instead.">
<!ENTITY torConnect.breadcrumbAssist "ချိတ်ဆက်မှု အကူအညီ ">
<!ENTITY torConnect.breadcrumbLocation "တည်နေရာ အပြင်အဆင်များ ">
<!ENTITY torConnect.breadcrumbTryBridge "တံတား ကို ကြိုးစားချိတ်ဆက်ပါ ။ ">
<!ENTITY torConnect.automatic "အလိုအလျောက်">
<!ENTITY torConnect.selectCountryRegion "တိုင်းပြည် သို့မဟုတ် ဒေသကို ရွေးချယ်ပါ ။ ">
<!ENTITY torConnect.frequentLocations "ရွေးချယ်လေ့ရှိသော တည်နေရာများ">
<!ENTITY torConnect.otherLocations "အခြား တည်နေရာများ">
<!ENTITY torConnect.restartTorBrowser "Tor ဘရောင်ဇာကို ပြန်လည်စတင်မယ် ။ ">
<!ENTITY torConnect.configureConnection "ချိတ်ဆက်မှုကို ချိန်ညိပါ ။ ">
<!ENTITY torConnect.viewLog "မှတ်တမ်းများ ကို ကြည့်ရန် ">
<!ENTITY torConnect.tryAgain "ထပ်မံလုပ်ဆောင်ပါ။">
<!ENTITY torConnect.offline "အင်တာနက်ကို မချိတ်ဆက်နိုင်ပါ ။ ">
<!ENTITY torConnect.connectMessage "Tor အပြင်အဆင်များကို ပြောင်းလဲခြင်းများသည် သင် ချိတ်ဆက်မှု မပြုမခြင်း သက်ရောက်မည်မဟုတ်ပါ ။ ">
<!ENTITY torConnect.tryAgainMessage "Tor ဘရောင်ဇာ သည် Tor ကွန်ရက် ကို ချိတ်ဆက်မှု တည်ဆောက်ရန် မအောင်မြင်ပါ။">
<!ENTITY torConnect.yourLocation "သင်၏ တည်နေရာ ">
<!ENTITY torConnect.tryBridge "တံတားကို ကြိုးစားချိတ်ဆက်ကြည့်ပါ ။ ">
<!ENTITY torConnect.autoBootstrappingFailed "အလိုအလျောက် ချိန်ညှိခြင်း မအောင်မြင်ပါ ။ ">
<!ENTITY torConnect.autoBootstrappingFailed "အလိုအလျောက် ချိန်ညှိခြင်း မအောင်မြင်ပါ ။ ">
<!ENTITY torConnect.cannotDetermineCountry "အသုံးပြုသူ ၏  တိုင်းပြည်ကို ဆုံးဖြတ်နိုင်ခြင်း မရှိပါ ။ ">
<!ENTITY torConnect.noSettingsForCountry "သင်၏ တည်နေရာအတွက် အပြင်အဆင်များ မရရှိနိုင်ပါ ။ ">
