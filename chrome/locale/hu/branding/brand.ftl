# For Tor Browser, we use a new file (different than the brand.ftl file
# that is used by Firefox) to avoid picking up the -brand-short-name values
# that Mozilla includes in the Firefox language packs.

-brand-shorter-name = Tor Böngésző
-brand-short-name = Tor Böngésző
-brand-full-name = Tor Böngésző
# This brand name can be used in messages where the product name needs to
# remain unchanged across different versions (Nightly, Beta, etc.).
-brand-product-name = Tor Böngésző
-vendor-short-name = Tor Project
trademarkInfo = A 'Tor' és az 'Onion Logo' a Tor Project, Inc. bejegyzett védjegyei.
